import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Body from './components/Body';
import Make from './components/Make';

function App() {
  return (
    <div className="App">
      <Header />
      <Body />
      <Make />
      <Footer />
    </div>
  );
}

export default App;
