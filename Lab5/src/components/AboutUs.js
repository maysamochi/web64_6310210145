import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';

function AboutUs(props){



    return(
        <Box sx={{width: "60%"}}>
            <Paper elevation={3}>
                <h2> จัดทำโดย: {props.name} </h2>
                <h3> ติดต่อติณณภพได้ ที่ {props.address} </h3>
                <h3> บ้านติณณภพอ อยู่ที่ {props.province} </h3>
            </Paper>
        </Box>
    
    );


}

export default AboutUs;