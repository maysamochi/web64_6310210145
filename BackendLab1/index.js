const express = require('express')
const app = express()
const port = 4000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post('/bmi', (req, res) =>{

    let weight = parseFloat(req.query.weight)
    let height = parseFloat(req.query.height)
    var result = {}

    if ( !isNaN(weight) && !isNaN(height) ){
        let bmi = weight / (height * height)
        result = {
            "status" : 200,
            "bmi" : bmi
        }
    }else {

        result = {
            "status" : 400,
            "Message" : "Weight or Height is not number"
        }
    }
    res.send(JSON.stringify(result))
})


app.get('/hello', (req, res) => {
    res.send('Sawasdee '+ req.query.name)
  })


  app.get('/triangle', (req, res) => {
    let height = parseFloat(req.query.height)
    let base = parseFloat(req.query.base)
    var area = {}

    if ( !isNaN(height) && !isNaN(base) ){
        let triangle = 1/2 * height * base

        area = {
            "status" : 200,
            "Triangle Area" : triangle
        }
    }else {

        area = {
            "status" : 400,
            "Message" : "Height or Base is not number"
        }
    }
    res.send(JSON.stringify(area))
  })


  app.post('/score', (req, res) => {
    let score = parseFloat(req.query.score)
    var grade = {}
    if( score >= 80){
        grade = "A"
    }else if( score >= 75){
        grade = "B+"
    }else if( score >= 70){
            grade = "B"
    }else if( score >= 65){
            grade = "C+"
    }else if( score >= 60){
            grade = "C"
    }else if( score >= 55){
            grade = "D+"
    }else if( score >= 50){
            grade = "D"
    }else if( score < 50){
            grade = "E"
    }else {
        Grade = {
            "Message" : "eror"
        }
    }
    res.send(JSON.stringify(req.query.name + "ได้เกรด" + grade))
  })


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})