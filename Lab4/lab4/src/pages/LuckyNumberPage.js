import LuckyNumber from "../components/LuckyNumber";
import { useState } from "react";

function LuckyNumberPage() {

    const [ number, setNumber] = useState("");
    const [ result, setResult] = useState("0");


    function Number(){

        let n =parseInt(number);
        let result = n
        setResult(result);
        if (number == 69){
            setResult("ถูกแล้วจ้า")
        }else{
            setResult("ผิด")
        }
    }

    return(
        <div>
            กรุณาทายตัวเลขที่ต้องการระหว่าง 0-99: <input tpye= "text"
                            value ={number}
                            onChange={ (e) => { setNumber(e.target.value); }}  /> <br />
            <button onClick={   ()=>{   Number()  }   }> ทาย </button>
            { result != 0 &&
                <div>
                    <hr />
                    <LuckyNumber
                    result = {result}
                   />
                </div>
            }
            
        </div>
    );
}

export default LuckyNumberPage;