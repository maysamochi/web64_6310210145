import { Link } from "react-router-dom";

function Header (){

    return(

        <div align="left"> 
            ยินดีต้อนรับสู่เว็ปคำนวณ BMI:  
            <Link to="/">เครื่องคิดเลข</Link>
            &nbsp;
            <Link to="/about">ผู้จัดทำ</Link>
            &nbsp;
            <Link to="/luckynumber">Luckynumber</Link>
            <hr />
        </div>

    );

}
export default Header;